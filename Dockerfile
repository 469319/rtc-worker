FROM python:3.9

WORKDIR /app

RUN apt-get update && apt-get install -y \
        wget \
        gdal-bin \
        libgdal-dev \
    && wget https://download.esa.int/step/snap/9.0/installers/esa-snap_sentinel_unix_9_0_0.sh \
    && chmod +x esa-snap_sentinel_unix_9_0_0.sh \
    && ./esa-snap_sentinel_unix_9_0_0.sh -q \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY . .

RUN chmod +x update-snap.sh && ./update-snap.sh

RUN pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir -r requirements.txt

ENV PYTHONPATH="/app"

CMD ["python", "worker/worker.py" ]