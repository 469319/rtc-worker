import urllib3
from minio import Minio

from config.configuration import ConfigurationProvider


def configure_minio() -> Minio:
    configuration = ConfigurationProvider.get_config()
    access_key = configuration.MINIO_ACCESS_KEY
    secret_access_key = configuration.MINIO_SECRET_ACCESS_KEY
    client = initialize_minio_client(access_key, configuration, secret_access_key, None)
    return client


def initialize_minio_client(access_key, configuration, secret_access_key, session_token):
    http_client = urllib3.PoolManager(
        timeout=urllib3.util.Timeout(connect=10, read=None),
        maxsize=10,
        cert_reqs='CERT_REQUIRED',
        ca_certs=configuration.CA_CERT_LOCATION,
        retries=urllib3.Retry(
            total=5,
            backoff_factor=0.2,
            status_forcelist=[500, 502, 503, 504]
        )
    )
    client = Minio(
        configuration.MINIO_URL.lstrip("https://").lstrip("http://"),
        access_key=access_key,
        secret_key=secret_access_key,
        session_token=session_token,
        secure=True,
        http_client=http_client
    )
    return client


def create_bucket_if_not_exists(client: Minio, bucket_name: str):
    if not client.bucket_exists(bucket_name=bucket_name):
        client.make_bucket(bucket_name=bucket_name)
