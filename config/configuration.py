import os
from dataclasses import dataclass, field
from functools import partial


@dataclass
class Configuration:
    MINIO_URL: str = field(default_factory=partial(os.environ.get, "MINIO_URL", "localhost:9001"))
    CA_CERT_LOCATION: str = field(default_factory=partial(os.environ.get, "CA_CERT_LOCATION", "/tls/tls.crt"))
    OAUTH_CLIENT_ID: str = field(default_factory=partial(os.environ.get, "CLIENT_ID", "client"))
    OAUTH_CLIENT_SECRET: str = field(default_factory=partial(os.environ.get, "CLIENT_SECRET", "secret"))
    BROKER_URLS: str = field(default_factory=partial(os.environ.get, "BROKERS", "localhost:8000"))
    AUTH_PROVIDER_URL: str = field(default_factory=partial(os.environ.get, "AUTH_PROVIDER_URL", "localhost:9000"))
    TLS_KEY_LOCATION: str = field(default_factory=partial(os.environ.get, "TLS_KEY_LOCATION", "/tls/tls.crt"))
    TLS_CERT_LOCATION: str = field(default_factory=partial(os.environ.get, "TLS_CERT_LOCATION", "/tls/tls.key"))
    CESNET_USERNAME: str = field(default_factory=partial(os.environ.get, "CESNET_USERNAME", "username"))
    CESNET_PASSWORD: str = field(default_factory=partial(os.environ.get, "CESNET_PASSWORD", "password"))
    MINIO_ACCESS_KEY: str = field(default_factory=partial(os.environ.get, "MINIO_ACCESS_KEY", ""))
    MINIO_SECRET_ACCESS_KEY: str = field(default_factory=partial(os.environ.get, "MINIO_SECRET_ACCESS_KEY", ""))


class ConfigurationProvider:
    __configuration = Configuration()

    @staticmethod
    def get_config() -> Configuration:
        return ConfigurationProvider.__configuration

