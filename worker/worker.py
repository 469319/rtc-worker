import rtc
import time
import os
import logging
import rtc_utils

from config.shared import configure_minio, create_bucket_if_not_exists
import minio_utils
from io import BytesIO
from constants.constants import ProductStatus, INPUT_BUCKET, OUTPUT_BUCKET, RESOURCES_BUCKET, MAX_STORAGE_SIZE, \
    EXPECTED_MAX_PRODUCT_SIZE


logger = logging.getLogger(__name__)


def radiometric_terrain_correction(product_identifier):
    logger.info(f"About to process product {product_identifier}")
    result = rtc.rtc_product(product_identifier)
    cwdir = os.getcwd()
    file_exists = os.path.exists(result)
    logger.info(f"After processing cwdir {cwdir} , result zip file {result} exists: {file_exists}")
    return result


def process_first_in_queue(minio_client):
    logger.info("Looking for an object to process...")

    while minio_utils.get_current_storage_size(minio_client) > MAX_STORAGE_SIZE - EXPECTED_MAX_PRODUCT_SIZE:
        oldest_object = minio_utils.get_oldest_object(minio_client, OUTPUT_BUCKET)
        logger.info(f"Removing oldest object for capacity reason: {oldest_object.object_name}")
        minio_client.remove_object(OUTPUT_BUCKET, oldest_object.object_name)

    object_to_process = minio_utils.get_oldest_object(minio_client, INPUT_BUCKET)

    if object_to_process is None:
        logger.info(f"Queue is currently empty (no oldest object found)")
        return

    metadata = rtc_utils.filter_dict_by_key_prefix(object_to_process.metadata, "X-Amz-Meta-")
    metadata["X-Amz-Meta-Status"] = ProductStatus.IN_PROGRESS.value

    minio_client.put_object(OUTPUT_BUCKET, object_to_process.object_name, BytesIO(b''), length=0, metadata=metadata)
    logger.info(f"Added {object_to_process.object_name} to {OUTPUT_BUCKET}")

    minio_client.remove_object(INPUT_BUCKET, object_to_process.object_name)
    logger.info(f"Removed {object_to_process.object_name} from {INPUT_BUCKET}")

    # RTC processing
    try:
        zip_output_path = radiometric_terrain_correction(object_to_process.object_name)
    except Exception as e:
        logger.info(f"ERROR: Processing of {object_to_process.object_name} failed")
        metadata["X-Amz-Meta-Status"] = ProductStatus.ERROR.value
        minio_client.put_object(OUTPUT_BUCKET, object_to_process.object_name, BytesIO(b''), length=0, metadata=metadata)
        logger.info(f"Updated status of {object_to_process.object_name} to Error")
        return

    metadata["X-Amz-Meta-Status"] = ProductStatus.FINISHED.value

    logger.info(f"Adding processed {object_to_process.object_name} to {OUTPUT_BUCKET}")
    minio_client.fput_object(
        bucket_name=OUTPUT_BUCKET,
        object_name=object_to_process.object_name,
        file_path=zip_output_path,
        metadata=metadata
    )
    logger.info(f"Replaced placeholder with actual zip file {object_to_process.object_name} in {OUTPUT_BUCKET}")

    os.system("rm -rf S1*")
    rtc_utils.log_and_print_files_and_directories()


def main():
    logging.basicConfig(level=logging.DEBUG)
    logger.info("Starting worker")

    minio_client = configure_minio()
    create_bucket_if_not_exists(minio_client, INPUT_BUCKET)
    create_bucket_if_not_exists(minio_client, OUTPUT_BUCKET)
    create_bucket_if_not_exists(minio_client, RESOURCES_BUCKET)

    minio_utils.clear_stuck_objects(minio_client, OUTPUT_BUCKET)

    logging.info("Downloading DEM from resources bucket")
    minio_client.fget_object(RESOURCES_BUCKET, "dem.tif", os.getcwd() + "/dem.tif")
    logging.info(f"Downloaded DEM from resources bucket to {os.getcwd()}/dem.tif")
    rtc_utils.log_and_print_files_and_directories()

    while True:
        try:
            process_first_in_queue(minio_client)
        except Exception as e:
            logger.error(e)
        time.sleep(60)


if __name__ == '__main__':
    main()
