import zipfile
import logging as logger
import os
from constants.constants import ALL_STEPS_APPLIED_SUFFIX


def filter_dict_by_key_prefix(input_dict, prefix):
    return {key: value for key, value in input_dict.items() if key.startswith(prefix)}


def zip_processed_files(identifier):
    output_filename = f"{identifier}_rtc.zip"
    filenames = [
        f"{identifier}{ALL_STEPS_APPLIED_SUFFIX}_Gamma0_VH_db.tif",
        f"{identifier}{ALL_STEPS_APPLIED_SUFFIX}_Gamma0_VV_db.tif",
        f"{identifier}{ALL_STEPS_APPLIED_SUFFIX}_projectedLocalIncidenceAngle_db.tif",
    ]

    with zipfile.ZipFile(output_filename, 'w') as zipf:
        for file in filenames:
            logger.info(f"Adding {file} into {output_filename}")
            zipf.write(file, arcname=file)

    logger.info(f"Archive {output_filename} has been created.")
    return output_filename


def log_and_print_files_and_directories():
    cwd = os.getcwd()
    logger.info(f"----- Current Working Directory: {cwd} -----")
    items = os.listdir(cwd)
    for item in items:
        if os.path.isdir(item):
            logger.info(f"      Directory: {item}")
        else:
            logger.info(f"      File: {item}")


# For testing purposes
def create_large_file(filename, size_in_gb):
    size_in_bytes = size_in_gb * (1024 ** 3)

    buffer_size = 1024 * 1024
    buffer = b'\x00' * buffer_size

    with open(filename, 'wb') as f:
        for _ in range(0, size_in_bytes, buffer_size):
            f.write(buffer)