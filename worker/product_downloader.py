import requests
from urllib.parse import urlencode
import logging as logger

from config.configuration import ConfigurationProvider

logger.basicConfig(level=logger.DEBUG)

dhusAuth = None
dhusIntegrity = None


def login():
    global dhusAuth, dhusIntegrity

    login_url = 'https://dhr1.cesnet.cz//login'
    login_headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    login_response = requests.post(login_url, data=urlencode({
        'login_username': ConfigurationProvider.get_config().CESNET_USERNAME,
        'login_password': ConfigurationProvider.get_config().CESNET_PASSWORD
    }), headers=login_headers)

    if login_response.status_code == 200:
        dhusAuth = login_response.cookies.get('dhusAuth')
        dhusIntegrity = login_response.cookies.get('dhusIntegrity')


def find_product_uuid(identifier):
    if dhusAuth and dhusIntegrity:

        product_url = f'https://dhr1.cesnet.cz/api/stub/products?filter={identifier}&offset=0&limit=25&sortedby=ingestiondate&order=desc'
        product_headers = {
            'Cookie': f'dhusAuth={dhusAuth}; dhusIntegrity={dhusIntegrity}',
            'Accept': 'application/json'
        }
        product_response = requests.get(product_url, headers=product_headers)

        if product_response.status_code == 200:
            response_json = product_response.json()
            products = response_json.get('products', [])
            if products:
                first_product = products[0]
                logger.info(f'Found product {first_product["uuid"]}')
                return first_product["uuid"]
            else:
                logger.info("No products found.")
        else:
            logger.info(f'Failed to find product, status code: {product_response.status_code}')
    else:
        logger.info('Authentication credentials are not set.')


def download_product(identifier):
    if dhusAuth and dhusIntegrity:

        uuid = find_product_uuid(identifier)

        products_url = f'https://dhr1.cesnet.cz/odata/v1/Products(\'{uuid}\')/$value'
        products_headers = {
            'Cookie': f'dhusAuth={dhusAuth}; dhusIntegrity={dhusIntegrity}',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
            'Accept-Encoding': 'gzip, deflate, br'
        }

        products_response = requests.get(products_url, headers=products_headers)

        if products_response.status_code == 200:
            with open(f"{identifier}.zip", 'wb') as file:
                file.write(products_response.content)
            logger.info('Product downloaded successfully.')
        else:
            logger.info(f'Failed to download product, status code: {products_response.status_code}')
    else:
        logger.info('Authentication credentials are not set.')


def login_and_download_product(identifier):
    login()
    download_product(identifier)
