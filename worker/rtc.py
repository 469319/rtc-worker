#!/usr/bin/python3
###################################################################
#
#  Alaska Satellite Facility DAAC
#
#  Sentinel Radiometric Terrain Correction using Sentinel Toolbox (SNAP)
#
###################################################################
#
# Copyright (C) 2016 Alaska Satellite Facility
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
###################################################################
import sys
import os
import re
import numpy
import datetime
import time
import product_downloader
import rtc_utils
import logging as logger

logger.basicConfig(level=logger.DEBUG)

global baseSNAP

###################################################################
#  SET THIS VARIABLE
#
#  In order to run this program, you must set the variable baseSNAP
#  to point to the gpt executable on your system.
#
###################################################################
baseSNAP = '/usr/local/snap/bin/gpt '
###################################################################


def timestamp(date):
    return time.mktime(date.timetuple())


def execute_cmd(cmd):
    logger.info(f'Executing command: {cmd}')
    os.system(cmd)


def create_temp_dir(cwdir, product_identifier):
    tempDir = f'{cwdir}/{product_identifier}'
    if not os.path.exists(cwdir):
        execute_cmd(f'mkdir {cwdir}')
    if not os.path.exists(tempDir):
        execute_cmd(f'mkdir {tempDir}')
    return tempDir


# Apply precise orbit file
def apply_orbit():
    start_time = datetime.datetime.now()
    cmd = f'{baseSNAP} Apply-Orbit-File -t {tempDir}/{product_identifier}_OB -PcontinueOnFail=\"true\" -PorbitType=\'Sentinel Precise (Auto Download)\' {input_file}'
    logger.info('Applying Precise Orbit file')
    execute_cmd(cmd)
    logger.info(f'Time to fix orbit: {timestamp(datetime.datetime.now()) - timestamp(start_time)}')
    return f'{product_identifier}_OB.dim'


# Apply thermal noise removal
def apply_tnr(inData):
    start_time = datetime.datetime.now()
    cmd = f'{baseSNAP} ThermalNoiseRemoval -t {tempDir}/{product_identifier}_OB_TNR -SsourceProduct={tempDir}/{inData}'
    logger.info('Applying Thermal Noise Removal')
    execute_cmd(cmd)
    logger.info(f'Time Thermal Noise Removal: {timestamp(datetime.datetime.now()) - timestamp(start_time)}')
    return f'{product_identifier}_OB_TNR.dim'


# Apply calibration
def apply_cal(inData):
    start_time = datetime.datetime.now()
    cmd = f'{baseSNAP} Calibration -PoutputBetaBand=true -PoutputSigmaBand=false -t {tempDir}/{product_identifier}_OB_TNR_CAL -Ssource={tempDir}/{inData}'
    logger.info('Applying Calibration')
    execute_cmd(cmd)
    logger.info(f'Time for Calibration: {timestamp(datetime.datetime.now()) - timestamp(start_time)}')
    return f'{product_identifier}_OB_TNR_CAL.dim'


# Apply terrain flattening
def apply_tf(inData):
    start_time = datetime.datetime.now()

    DEM = ' -PdemName=\"SRTM 1Sec HGT\"'
    if extDEM is not None:
        DEM = f' -PdemName=\"External DEM\" -PexternalDEMFile={extDEM} -PexternalDEMNoDataValue=0'

    cmd = f'{baseSNAP} Terrain-Flattening -t {tempDir}/{product_identifier}_OB_TNR_CAL_TF -Ssource={tempDir}/{inData} {DEM}'
    logger.info('Applying Terrain Flattening -- This will take some time')
    execute_cmd(cmd)
    logger.info(f'Time for Terrain Flattening: {timestamp(datetime.datetime.now()) - timestamp(start_time)}')
    return f'{product_identifier}_OB_TNR_CAL_TF.dim'


# Apply terrain correction
def apply_tc(inData, hemisphere, zone):
    start_time = datetime.datetime.now()

    DEM = ' -PdemName=\"SRTM 1Sec HGT\"'
    if extDEM is not None:
        DEM = f' -PdemName=\"External DEM\" -PexternalDEMFile={extDEM} -PexternalDEMNoDataValue=0'

    if hemisphere == "S":
        map_projection = '-PmapProjection=EPSG:327%02d' % zone
    else:
        map_projection = '-PmapProjection=EPSG:326%02d' % zone

    cmd = f'{baseSNAP} Terrain-Correction -t {tempDir}/{product_identifier}_OB_TNR_CAL_TF_TC -Ssource={tempDir}/{inData} -PsaveDEM=true -PsaveProjectedLocalIncidenceAngle=true -PpixelSpacingInMeter={pixsiz} {map_projection} {DEM}'
    logger.info('Applying Terrain Correction -- This will take some time')
    execute_cmd(cmd)
    logger.info(f'Time for Terrain Correction: {timestamp(datetime.datetime.now()) - timestamp(start_time)}')
    return f'{product_identifier}_OB_TNR_CAL_TF_TC.dim'


# Apply linear to dB
def apply_dB(inData):
    start_time = datetime.datetime.now()
    cmd = f'{baseSNAP} LinearToFromdB -t {tempDir}/{product_identifier}_OB_TNR_CAL_TF_TC_dB -Ssource={tempDir}/{inData}'
    logger.info('Applying LinearToFromdB')
    execute_cmd(cmd)
    logger.info(f'Time for LinearToFromdB: {timestamp(datetime.datetime.now()) - timestamp(start_time)}')
    return f'{product_identifier}_OB_TNR_CAL_TF_TC_dB.dim'


# Get the UTM zone, central meridian, and hemisphere
def getZone(inData):
    temp = inData.replace('.zip', '.SAFE')
    if not os.path.isdir(temp):
        cmd = "unzip %s" % inData
        print(cmd)
        os.system(cmd)
    back = os.getcwd()
    os.chdir(temp)
    os.chdir('annotation')

    paths = os.listdir(os.getcwd())
    for temp in paths:
        if os.path.isfile(temp):
            toread = temp
            break
    f = open(toread, 'r')

    min_lon = 180
    max_lon = -180
    for line in f:
        m = re.search('<longitude>(.+?)</longitude>', line)
        if m:
            lon = float(m.group(1))
            if lon > max_lon:
                max_lon = lon
            if lon < min_lon:
                min_lon = lon
    f.close()
    print("Found max_lon of %s" % max_lon)
    print("Found min_lon of %s" % min_lon)
    center_lon = (float(min_lon) + float(max_lon)) / 2.0
    print("Found center_lon of %s" % center_lon)
    zone = int(float(lon)+180)/6 + 1
    print("Found UTM zone of %s" % zone)
    central_meridian = (zone-1)*6-180+3
    print("Found central meridian of %s" % central_meridian)

    f = open(toread, 'r')

    min_lat = 180
    max_lat = -180
    for line in f:
        m = re.search('<latitude>(.+?)</latitude>', line)
        if m:
            lat = float(m.group(1))
            if lat > max_lat:
                max_lat = lat
            if lat < min_lat:
                min_lat = lat
    f.close()
    print("Found max_lat of %s" % max_lat)
    print("Found min_lat of %s" % min_lat)
    center_lat = (float(min_lat) + float(max_lat)) / 2.0
    print("Found center_lat of %s" % center_lat)
    if (center_lat < 0):
        hemi = "S"
    else:
        hemi = "N"
    print("Found hemisphere of %s" % hemi)

    os.chdir(back)
    return zone, central_meridian, hemi


def convert_to_geotiffs(last_step_dim):
    img_files = [file for file in os.listdir('.') if file.endswith('.img')]
    for filename in img_files:
        if os.path.isfile(filename):
            filename_out = filename.replace(".img", ".tif")

            translate_cmd = f'gdal_translate -of GTiff {filename} {filename_out}'
            execute_cmd(translate_cmd)

            move_cmd = f'mv {filename_out} ../{last_step_dim.replace(".dim", f"_{filename_out}")}'
            execute_cmd(move_cmd)
        else:
            logger.info(f'Skipping {filename} (file not found)')


def rtc_product(s1_product):
    start_time = datetime.datetime.now()

    global product_identifier, tempDir, input_file, cwdir, extDEM, cleanTemp, pixsiz
    cwdir = os.getcwd()

    product_identifier = s1_product
    input_file = cwdir + "/" + product_identifier + ".zip"
    extDEM = "/app/dem.tif"
    cleanTemp = True
    pixsiz = 10.0

    logger.info(f'Product identifier: {product_identifier}')
    logger.info(f'Input file: {input_file}')
    logger.info(f'Current working directory: {cwdir}')
    logger.info(f'DEM: {extDEM}')
    logger.info(f'CLEANUP: {cleanTemp}')
    logger.info(f'RESOLUTION: {pixsiz}')

    # Download product
    product_downloader.login_and_download_product(product_identifier)

    # Apply processing steps
    tempDir = create_temp_dir(cwdir, product_identifier)
    logger.info(f'Processing in directory {tempDir}')
    (zone, central_meridian, hemisphere) = getZone(input_file)
    logger.info(f'Zone: {zone}, Hemisphere: {hemisphere}, Central Meridian: {central_meridian}')

    result = apply_orbit()
    result = apply_tnr(result)
    execute_cmd(f'rm -r {tempDir}/*OB.data; rm {tempDir}/*OB.dim')
    result = apply_cal(result)
    execute_cmd(f'rm -r {tempDir}/*TNR.data; rm {tempDir}/*TNR.dim')
    result = apply_tf(result)
    execute_cmd(f'rm -r {tempDir}/*CAL.data; rm {tempDir}/*CAL.dim')
    result = apply_tc(result, hemisphere, zone)
    execute_cmd(f'rm -r {tempDir}/*TF.data; rm {tempDir}/*TF.dim')
    result = apply_dB(result)
    execute_cmd(f'rm -r {tempDir}/*TC.data; rm {tempDir}/*TC.dim')

    # Convert files from the last step to GeoTIFFs
    os.chdir(f'{tempDir}/{result.replace(".dim", ".data")}')
    logger.info(f"tempDir : {tempDir}")
    rtc_utils.log_and_print_files_and_directories()
    convert_to_geotiffs(result)
    os.chdir(f'{tempDir}')

    # Add GeoTiffs to a ZIP archive
    zip_output_filename = rtc_utils.zip_processed_files(product_identifier)
    execute_cmd(f"mv {zip_output_filename} {cwdir}")

    # Cleanup
    os.chdir(cwdir)
    rtc_utils.log_and_print_files_and_directories()

    logger.info(f'Processing finished. Total processing time: {timestamp(datetime.datetime.now()) - timestamp(start_time)}')
    return zip_output_filename
