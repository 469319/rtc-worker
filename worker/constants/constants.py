from enum import Enum


class ProductStatus(Enum):
    NOT_PROCESSED = "Not Processed"
    ERROR = "Error"
    QUEUED = "Queued"
    IN_PROGRESS = "In Progress"
    FINISHED = "Finished"


INPUT_BUCKET = "rtc-queue"
OUTPUT_BUCKET = "rtc-output"
RESOURCES_BUCKET = "rtc-resources"

MAX_STORAGE_SIZE = 300 * 1024 * 1024 * 1024  # 300 GB
EXPECTED_MAX_PRODUCT_SIZE = 10 * 1024 * 1024 * 1024  # 10 GB


ALL_STEPS_APPLIED_SUFFIX = "_OB_TNR_CAL_TF_TC_dB"
