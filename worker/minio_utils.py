from io import BytesIO
from constants.constants import ProductStatus, INPUT_BUCKET, OUTPUT_BUCKET, RESOURCES_BUCKET
from rtc_utils import filter_dict_by_key_prefix
from datetime import datetime, timezone
import logging as logger


def clear_stuck_objects(minio_client, bucket):
    objects = minio_client.list_objects(bucket, include_user_meta=True)
    for obj in objects:
        metadata = filter_dict_by_key_prefix(obj.metadata, "X-Amz-Meta-")
        if metadata["X-Amz-Meta-Status"] == ProductStatus.IN_PROGRESS.value:
            logger.info(f"Found stuck IN-PROGRESS product {obj.object_name} in {bucket}")
            metadata["X-Amz-Meta-Status"] = ProductStatus.ERROR.value

            minio_client.remove_object(bucket, obj.object_name)
            logger.info(f"Removed {obj.object_name} from {bucket}")

            minio_client.put_object(bucket, obj.object_name, BytesIO(b''), length=0, metadata=metadata)
            logger.info(f"Added {obj.object_name} to {bucket}")


def get_oldest_object(minio_client, bucket):
    objects = minio_client.list_objects(bucket, include_user_meta=True)
    oldest_object = None
    oldest_time = datetime.max.replace(tzinfo=timezone.utc)

    for obj in objects:
        if obj.last_modified < oldest_time:
            oldest_time = obj.last_modified
            oldest_object = obj
    return oldest_object


def get_current_storage_size(minio_client):
    buckets = [INPUT_BUCKET, OUTPUT_BUCKET, RESOURCES_BUCKET]
    total_size = 0
    for bucket in buckets:
        objects = minio_client.list_objects(bucket)
        for obj in objects:
            total_size += obj.size
    logger.info(f"Current total storage size: {total_size} bytes")
    return total_size
